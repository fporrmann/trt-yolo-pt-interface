/* 
 *  File: YoloPTTRT.h
 *  Copyright (c) 2023 Florian Porrmann
 *  
 *  MIT License
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 *  
 */

#pragma once

#include "Timer.h"
#include "Utils.h"

// == TensorRT includes ==
#include "buffers.h"
#include "logger.h"
#include "parserOnnxConfig.h"

#include <NvInfer.h>
#include <NvInferPlugin.h>
// == TensorRT includes ==

// == OpenCV includes ==
#include <opencv2/ml.hpp>
#include <opencv2/opencv.hpp>
// == OpenCV includes ==

#include <algorithm>
#include <experimental/filesystem>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

namespace fs = std::experimental::filesystem;

DEFINE_EXCEPTION(YoloPTTRTException)

class YoloPTTRT
{
	inline static const bool FORCE_REBUILD = false;

	template<typename T>
	using InferUniquePtr = std::unique_ptr<T, samplesCommon::InferDeleter>;

public:
	struct YoloPTResult
	{
		float x;
		float y;
		float w;
		float h;
		int32_t classID;
		float classProb;

		float Conf() const
		{
			return classProb;
		}

		uint32_t ClassID() const
		{
			return static_cast<uint32_t>(classID);
		}

		friend std::ostream& operator<<(std::ostream& os, const YoloPTResult& dt)
		{
			os << string_format("x=%f - y=%f - w=%f - h=%f - cID=%d - cProb=%f", dt.x, dt.y, dt.w, dt.h, static_cast<int32_t>(dt.classID), dt.classProb);
			return os;
		}
	};

	using YoloPTResults = std::vector<YoloPTResult>;

public:
	YoloPTTRT(const std::string& onnxFile, const std::string& engineFile, const std::string& classFile,
			  const int32_t& dlaCore = 0, const bool& useFP16 = false, const bool& autoLoad = false,
			  const float& threshold = 0.3f, const cv::Scalar borderColor = cv::Scalar(0, 0, 0), const bool& bchw = true) :
		m_onnxFile(onnxFile),
		m_engineFile(engineFile),
		m_dlaCore(dlaCore),
		m_useFP16(useFP16),
		m_threshold(threshold),
		m_borderColor(borderColor),
		m_bchw(bchw)
	{
		parseClassFile(classFile);
		if (autoLoad)
			buildOrLoadEngine(FORCE_REBUILD);
	}

	void InitEngine()
	{
		buildOrLoadEngine(FORCE_REBUILD);
	}

	void SetVerbose(const bool& en = true)
	{
		m_verbose = en;
	}

	void SetThreshold(const float& threshold)
	{
		m_threshold = threshold;
	}

	YoloPTResults Infer(const cv::Mat& img)
	{
		m_imgSize = img.size();

		Timer timer;

		// ==== Inference ====
		timer.Start();

		processInput(img);

		// Copy data from host input buffers to device input buffers
		m_pBuffers->copyInputToDevice();

		// Execute the inference work
		if (!m_pContext->executeV2(m_pBuffers->getDeviceBindings().data()))
			return YoloPTResults();

		// Copy data from device output buffers to host output buffers
		m_pBuffers->copyOutputToHost();

		// ==== Inference ====

		YoloPTResults results = processOutput(img);

		// std::cout << "Inference-Timer: " << timer << std::endl;

		return results;
	}

	std::size_t GetClassCount() const
	{
		return m_classes.size();
	}

	const std::vector<std::string>& Classes() const
	{
		return m_classes;
	}

	std::string ClassName(const std::size_t& classID) const
	{
		if (m_classes.size() < classID)
			return string_format("INVALID_CLASSID: %s", classID);
		return m_classes.at(classID);
	}

private:
	struct ResizeParams
	{
		cv::Size scaleFactor;
		cv::Size borderSize;
	};

	void parseClassFile(const std::string& classFile)
	{
		m_classes.clear();
		std::ifstream f(classFile);
		if (!f.is_open())
			throw(YoloPTTRTException(string_format("Failed to load class file: %s", classFile.c_str())));

		std::string line;
		while ((std::getline(f, line)))
			m_classes.push_back(line);
	}

	void setupOutputMap()
	{
		for (int32_t i = 0; i < m_pEngine->getNbBindings(); i++)
		{
			std::string name    = std::string(m_pEngine->getBindingName(i));
			m_outputLayer[name] = 1;
			nvinfer1::Dims d    = m_pEngine->getBindingDimensions(i);

			if (m_pEngine->bindingIsInput(i))
			{
				m_inputLayer  = name;
				m_inputHeight = (m_bchw ? d.d[2] : d.d[1]);
				m_inputWidth  = (m_bchw ? d.d[3] : d.d[2]);
			}
			else
			{
				for (int32_t j = 0; j < d.nbDims; j++)
					m_outputLayer[name] *= d.d[j];
			}
		}
	}

	void buildOrLoadEngine(const bool& forceRebuild = false)
	{
		if (!forceRebuild && fileExists(m_engineFile))
			loadEngine();
		else
			buildEngine();

		// Create RAII buffer manager object
		m_pBuffers = std::make_shared<samplesCommon::BufferManager>(m_pEngine);
	}

	void loadEngine()
	{
		std::cout << "Loading serialized engine ... " << std::flush;

		std::ifstream file(m_engineFile, std::ios::binary);
		if (!file)
			throw(YoloPTTRTException(string_format("[LoadEngine] Failed to open Engine File: %s", m_engineFile)));

		file.seekg(0, file.end);
		std::size_t size = file.tellg();
		file.seekg(0, file.beg);

		std::vector<char> engineData(size);
		file.read(engineData.data(), size);
		file.close();

		InferUniquePtr<nvinfer1::IRuntime> pRuntime{ nvinfer1::createInferRuntime(TrtLog::gLogger.getTRTLogger()) };
		if (!pRuntime)
			throw(YoloPTTRTException("Failed to create InferRuntime"));

		initLibNvInferPlugins(&TrtLog::gLogger.getTRTLogger(), "");

		if (m_dlaCore >= 0)
		{
			std::cout << " - Enabling DLACore=" << m_dlaCore << " - " << std::flush;
			pRuntime->setDLACore(m_dlaCore);
		}

		m_pEngine = std::shared_ptr<nvinfer1::ICudaEngine>(pRuntime->deserializeCudaEngine(engineData.data(), size, nullptr), samplesCommon::InferDeleter());
		if (!m_pEngine)
			throw(YoloPTTRTException("Failed to deserialize Engine"));

		setupOutputMap();

		m_pContext = InferUniquePtr<nvinfer1::IExecutionContext>(m_pEngine->createExecutionContext());
		if (!m_pContext)
			throw(YoloPTTRTException("Failed to create Execution Context"));

		std::cout << "Done" << std::endl;
	}

	void buildEngine()
	{
		auto builder = InferUniquePtr<nvinfer1::IBuilder>(nvinfer1::createInferBuilder(TrtLog::gLogger.getTRTLogger()));
		if (!builder)
			throw(YoloPTTRTException("Failed to create Builder"));

		const uint32_t explicitBatch = 1U << static_cast<uint32_t>(nvinfer1::NetworkDefinitionCreationFlag::kEXPLICIT_BATCH);

		auto network = InferUniquePtr<nvinfer1::INetworkDefinition>(builder->createNetworkV2(explicitBatch));
		if (!network)
			throw(YoloPTTRTException("Failed to create Network"));

		auto config = InferUniquePtr<nvinfer1::IBuilderConfig>(builder->createBuilderConfig());
		if (!config)
			throw(YoloPTTRTException("Failed to create BuilderConfig"));

		auto parser = InferUniquePtr<nvonnxparser::IParser>(nvonnxparser::createParser(*network, TrtLog::gLogger.getTRTLogger()));
		if (!parser)
			throw(YoloPTTRTException("Failed to create Parser"));

		if (!parser->parseFromFile(m_onnxFile.c_str(), static_cast<int>(TrtLog::Severity::kERROR)))
			throw(YoloPTTRTException("Failed to parse ONNX File"));

		builder->setMaxBatchSize(1);
		config->setMaxWorkspaceSize(((size_t)1) << 30);
		config->setFlag(nvinfer1::BuilderFlag::kGPU_FALLBACK);

		if (m_useFP16)
			config->setFlag(nvinfer1::BuilderFlag::kFP16);

		samplesCommon::enableDLA(builder.get(), config.get(), m_dlaCore);

		m_pEngine = std::shared_ptr<nvinfer1::ICudaEngine>(builder->buildEngineWithConfig(*network, *config), samplesCommon::InferDeleter());
		if (!m_pEngine)
			throw(YoloPTTRTException("Failed to create Build Engine"));

		m_pContext = InferUniquePtr<nvinfer1::IExecutionContext>(m_pEngine->createExecutionContext());
		if (!m_pContext)
			throw(YoloPTTRTException("Failed to create Execution Context"));

		std::cout << "Writing engine file to disk ... " << std::flush;
		std::ofstream engineFile(m_engineFile, std::ios::binary);
		if (!engineFile)
			throw(YoloPTTRTException(string_format("[SaveEngine] Failed to open Engine File: %s", m_engineFile)));

		InferUniquePtr<nvinfer1::IHostMemory> pSerializedEngine{ m_pEngine->serialize() };
		if (!pSerializedEngine)
			throw(YoloPTTRTException("Failed to serialize Engine"));

		engineFile.write(static_cast<char*>(pSerializedEngine->data()), pSerializedEngine->size());
		engineFile.close();

		setupOutputMap();
		std::cout << "Done" << std::endl;
	}

	void processInput(const cv::Mat& image)
	{
		// ==== Pre-Process Image ====
		std::vector<float> data;
		cv::Mat scaledImg;
		cv::Mat imgConv;
		cv::Mat imgConv2;

		if ((m_inputWidth != image.cols) || (m_inputHeight != image.rows))
			scaledImg = resizeKeepAspectRatio(image, cv::Size(m_inputWidth, m_inputHeight), m_borderColor, m_resizeParams);
		else
		{
			m_resizeParams = { {m_inputWidth, m_inputHeight}, {0, 0} };
			scaledImg      = image;
		}

		cv::cvtColor(scaledImg, imgConv, cv::COLOR_BGR2RGB);
		imgConv.convertTo(imgConv2, CV_32FC3, 1 / 255.0);

		std::vector<cv::Mat> channles(3);
		cv::split(imgConv2, channles);
		float* ptr1 = reinterpret_cast<float*>(channles[0].data);
		float* ptr2 = reinterpret_cast<float*>(channles[1].data);
		float* ptr3 = reinterpret_cast<float*>(channles[2].data);
		data.insert(data.end(), ptr1, ptr1 + m_inputWidth * m_inputHeight);
		data.insert(data.end(), ptr2, ptr2 + m_inputWidth * m_inputHeight);
		data.insert(data.end(), ptr3, ptr3 + m_inputWidth * m_inputHeight);

		// ==== Pre-Process Image ====

		float* hostInputBuffer = static_cast<float*>(m_pBuffers->getHostBuffer(m_inputLayer));
		std::memcpy(hostInputBuffer, data.data(), data.size() * sizeof(float));
	}

	YoloPTResults processOutput(const cv::Mat& img) const
	{
		YoloPTResults results;

		int32_t numDets      = 0;
		int32_t* pDetClasses = nullptr;
		float* pDetBoxes     = nullptr;
		float* pDetScores    = nullptr;

		for (const auto& [outLayer, gridSize] : m_outputLayer)
		{
			if (outLayer == "num_dets")
				numDets = *static_cast<int32_t*>(m_pBuffers->getHostBuffer(outLayer));
			else if (outLayer == "det_classes")
				pDetClasses = static_cast<int32_t*>(m_pBuffers->getHostBuffer(outLayer));
			else if (outLayer == "det_boxes")
				pDetBoxes = static_cast<float*>(m_pBuffers->getHostBuffer(outLayer));
			else if (outLayer == "det_scores")
				pDetScores = static_cast<float*>(m_pBuffers->getHostBuffer(outLayer));
		}

		for (int32_t i = 0; i < numDets; i++)
		{
			if (pDetScores[i] < m_threshold)
				continue;

			YoloPTResult result;
			result.classID   = pDetClasses[i];
			result.classProb = pDetScores[i];
			// Scale the detections back to the original image size
			result.x = (pDetBoxes[i * 4] - m_resizeParams.borderSize.width) / m_resizeParams.scaleFactor.width;
			result.y = (pDetBoxes[i * 4 + 1] - m_resizeParams.borderSize.height) / m_resizeParams.scaleFactor.height;
			result.w = (pDetBoxes[i * 4 + 2] - pDetBoxes[i * 4]) / m_resizeParams.scaleFactor.width;
			result.h = (pDetBoxes[i * 4 + 3] - pDetBoxes[i * 4 + 1]) / m_resizeParams.scaleFactor.height;

			results.push_back(result);
		}

//#define PRINT_DETECTIONS
#ifdef PRINT_DETECTIONS
		cv::Mat imgLocal = img.clone();
		for (const YoloPTResult& res : results)
		{
			float x = res.x * imgLocal.cols;
			float y = res.y * imgLocal.rows;
			float w = res.w * imgLocal.cols;
			float h = res.h * imgLocal.rows;
			float r = res.classProb;

			cv::rectangle(imgLocal, cv::Point(x, y), cv::Point(x + w, y + h), cv::Scalar(0, 255, 0));
			cv::putText(imgLocal, string_format("%s - %f", ClassName(res.classID).c_str(), r), cv::Point(x, y - 10), cv::FONT_HERSHEY_DUPLEX, 0.5, CV_RGB(255, 50, 50), 1);
		}

		cv::imwrite("out.jpg", imgLocal);
#endif

		return results;
	}

	static bool fileExists(const std::string& name)
	{
		std::ifstream f(name);
		return f.good();
	}

	// From: https://stackoverflow.com/a/40048370
	static cv::Mat resizeKeepAspectRatio(const cv::Mat& input, const cv::Size& dstSize, const cv::Scalar& bgColor, ResizeParams& params)
	{
		cv::Mat output;

		double h1 = dstSize.width * (input.rows / static_cast<double>(input.cols));
		double w2 = dstSize.height * (input.cols / static_cast<double>(input.rows));

		if (h1 <= dstSize.height)
			params.scaleFactor = cv::Size(dstSize.width, h1);
		else
			params.scaleFactor = cv::Size(w2, dstSize.height);

		cv::resize(input, output, params.scaleFactor);

		int32_t top   = (dstSize.height - output.rows) / 2;
		int32_t down  = (dstSize.height - output.rows + 1) / 2;
		int32_t left  = (dstSize.width - output.cols) / 2;
		int32_t right = (dstSize.width - output.cols + 1) / 2;

		params.borderSize = cv::Size(left, top);

		cv::copyMakeBorder(output, output, top, down, left, right, cv::BORDER_CONSTANT, bgColor);

		return output;
	}

private:
	std::string m_onnxFile;
	std::string m_engineFile;
	int32_t m_dlaCore;
	bool m_useFP16;

	std::shared_ptr<nvinfer1::ICudaEngine> m_pEngine         = nullptr;
	InferUniquePtr<nvinfer1::IExecutionContext> m_pContext   = nullptr;
	std::shared_ptr<samplesCommon::BufferManager> m_pBuffers = nullptr;

	int32_t m_inputWidth     = -1;
	int32_t m_inputHeight    = -1;
	std::string m_inputLayer = "";

	cv::Scalar m_borderColor;
	ResizeParams m_resizeParams = ResizeParams();

	std::map<std::string, uint32_t> m_outputLayer = std::map<std::string, uint32_t>();

	bool m_verbose     = false;
	cv::Size m_imgSize = cv::Size(0, 0);
	float m_threshold;
	bool m_bchw;
	std::vector<std::string> m_classes = std::vector<std::string>();
};
